<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Hiring;
use App\Models\HiringApply;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HiringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        // https://laravel.com/docs/8.x/requests
        // $request represente la requete contient toutes les informations de la requete qui appelera la fonction index

        $hirings = null;

        // recuperer le contenu du champs de recherche (le formulaire est en get alors le contenu de tout les champs sont passés dans l'url du genre RequestParam)
        $qs = $request->qs;

        // recuperer la valeur de filtrage
        $qd = $request->qd;

        if ($qd && $qd == 'new') {
            $hirings = Hiring::where([
                ['created_at', '>=', Carbon::now()->startOfDay()],
                ['created_at', '<=', Carbon::now()->endOfDay()],
            ])->orderBy('created_at', 'DESC');
        } else if ($qd && $qd == 'week') {
            $hirings = Hiring::where([
                ['created_at', '>=', Carbon::now()->startOfWeek()],
                ['created_at', '<=', Carbon::now()->endOfWeek()],
            ])->orderBy('created_at', 'DESC');
        } else if ($qd && $qd == 'month') {
            $hirings = Hiring::where([
                ['created_at', '>=', Carbon::now()->startOfMonth()],
                ['created_at', '<=', Carbon::now()->endOfMonth()],
            ])->orderBy('created_at', 'DESC');
        } else if ($qd && $qd == 'lastmonth') {
            $hirings = Hiring::where([
                ['created_at', '>=', Carbon::now()->subMonth()->startOfMonth()],
                ['created_at', '<=', Carbon::now()->subMonth()->endOfMonth()],
            ])->orderBy('created_at', 'DESC');
        } else {
            $hirings = Hiring::orderBy('created_at', 'DESC');
        }

        if ($qs && Str::length(trim($qs)) != 0) {
            $hirings = $hirings == null ? Hiring::where('poste', 'LIKE', '%' . $qs . '%')->orderBy('created_at', 'DESC')->paginate(10) : $hirings->where('poste', 'LIKE', '%' . $qs . '%')->paginate(10);
        } else {
            $hirings = $hirings == null ? Hiring::orderBy('created_at', 'DESC')->paginate(10) : $hirings->paginate(10);
        }

        $hirings = $hirings ?? Hiring::orderBy('created_at', 'DESC')->paginate(10);
        return view('hiring.index')->with(compact(['hirings', 'qs']));
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $hiring = Hiring::where('slug', $slug)->first();
        return view('hiring.show')->with(compact(['hiring']));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function apply(Request $request, $slug)
    {
        //Validation des données envoyées depuis le formulaire de création
        $validator = Validator::make($request->all(), [
            'name_input' => 'required|min:2|max:100',
            'surname_input' => 'required|min:2|max:100',
            'email_input' => 'required|email',
            'tel_input' => 'required|min:6',
            'cv_input' => 'required|mimes:pdf|max:10000',
            'last_d_input' => 'required|mimes:pdf|max:10000',
            'motivation_letter_input' => 'required|mimes:pdf|max:10000',
        ], [
            'name_input.required' => 'Veuillez fournir votre nom !',
            'email_input.required' => 'Veuillez fournir vos prénoms!',
            'email_input.required' => 'Veuillez fournir votre email !',
            'tel_input.required' => 'Veuillez fournir votre numéro de téléphone !',
            'cv_input.required' => 'Veuillez fournir votre CV en PDF !',
            'last_d_input.required' => 'Veuillez fournir votre dernier diplome en PDF !',
            'motivation_letter_input.required' => 'Veuillez fournir une lettre de motivation en PDF !',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->with('error', $validator->errors()->first());
        }

        $pathc = null;
        $pathld = null;
        $pathml = null;

        if ($request->hasFile('cv_input')) {
            $file = $request->file('cv_input');
            $originalname = $file->getClientOriginalName();
            $pathc = $file->storeAs('public/', $originalname);
        }

        if ($request->hasFile('last_d_input')) {
            $file = $request->file('last_d_input');
            $originalname = $file->getClientOriginalName();
            $pathld = $file->storeAs('public/', $originalname);
        }

        if ($request->hasFile('motivation_letter_input')) {
            $file = $request->file('motivation_letter_input');
            $originalname = $file->getClientOriginalName();
            $pathml = $file->storeAs('public/', $originalname);
        }

        HiringApply::create([
            'name' => $request->name_input,
            'surname' => $request->surname_input,
            'email' => $request->email_input,
            'phone' => $request->tel_input,
            'cv' => $pathc,
            'last_d' => $pathld,
            'motivation_letter' => $pathml
        ]);

        return redirect()->back()->with('success', 'Candidature enregistrée !.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function createApply($slug)
    {
        $hiring = Hiring::where('slug', $slug)->first();
        return view('hiring.apply')->with(compact(['hiring']));
    }
}
