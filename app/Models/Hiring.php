<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hiring extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable = ['poste', 'description', 'statut', 'type', 'slug'];
    protected $appends = ['publication_at'];

    public function getPublicationAtAttribute()
    {

        if ($this->created_at) {
            $e = explode(' ', $this->created_at);
            return join('/', array_reverse(explode('-', $e[0]))) . ' à ' . $e[1];
        }
        return null;
    }

    public function abilities()
    {
        return $this->hasMany(HiringAbility::class, 'hiring_id');
    }

    public function profiles()
    {
        return $this->hasMany(HiringRequiredProfile::class, 'hiring_id');
    }

    public function documents()
    {
        return $this->hasMany(HiringRequiredDocument::class, 'hiring_id');
    }

    public function responsabilities()
    {
        return $this->hasMany(HiringResponsibility::class, 'hiring_id');
    }
}
